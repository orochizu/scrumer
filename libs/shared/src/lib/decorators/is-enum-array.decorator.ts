import {
	registerDecorator,
	ValidationArguments,
	ValidationOptions,
} from 'class-validator';
import { isArray } from 'util';

const defaultMsg = 'property must be a valid array of enum';

export const IsEnumArray = (
	entity: Object,
	validationOptions?: ValidationOptions
) => {
	const options: ValidationOptions =
		!validationOptions || !validationOptions.message
			? {
					...validationOptions,
					message: defaultMsg,
			  }
			: validationOptions;

	return (object: Object, propertyName: string) => {
		registerDecorator({
			name: 'isArrayOfEnum',
			target: object.constructor,
			constraints: [entity],
			propertyName,
			options,
			validator: {
				validate(
					value: any,
					args?: ValidationArguments
				): Promise<boolean> | boolean {
					const enumObject = args.constraints[0];
					const enumValues = Object.keys(enumObject).filter(
						(key) => !isNaN(Number(key))
					);

					return (
						value &&
						isArray(value) &&
						value.every((val) => enumValues.includes(val))
					);
				},
			},
		});
	};
};
