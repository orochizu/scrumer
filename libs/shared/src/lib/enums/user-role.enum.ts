export enum UserRole {
	JUNIOR,
	REGULAR,
	SENIOR,
	LEAD,
}
