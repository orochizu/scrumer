import { BoardCreateDto } from '@scrumer/shared';

export const boardsMock: BoardCreateDto[] = [
	{
		name: 'BoardName0',
		ownerId: 4,
		membersIds: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
	},
	{
		name: 'BoardName1',
		ownerId: 8,
		membersIds: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
	},
	{
		name: 'BoardName2',
		ownerId: 12,
		membersIds: [11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
	},
	{
		name: 'BoardName3',
		ownerId: 16,
		membersIds: [11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
	},
	{
		name: 'BoardName4',
		ownerId: 20,
		membersIds: [11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
	},
];
