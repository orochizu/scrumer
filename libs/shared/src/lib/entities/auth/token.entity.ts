import { BeforeInsert, Column, Entity } from 'typeorm';
import { BaseEntity } from '../../abstract/base-entity';
import * as uuid from 'uuid';

@Entity({ name: 'tokens' })
export class Token extends BaseEntity {
	@Column({ nullable: false, length: 64 })
	uuid: string;

	@Column({ nullable: false })
	email: string;

	@Column({ nullable: false })
	expires?: Date;

	@Column({ nullable: false })
	created?: Date;

	@BeforeInsert()
	public generateToken() {
		this.uuid = uuid();
	}

	@BeforeInsert()
	public initDates() {
		this.created = new Date();
		this.expires = new Date();

		this.expires.setDate(this.created.getDate() + 1);
	}
}
