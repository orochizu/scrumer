import { BeforeInsert, Column, Entity } from 'typeorm';
import { genSaltSync, hashSync } from 'bcrypt';
import { BaseEntityDate } from '../../abstract/base-entity-date';
import { Exclude } from 'class-transformer';
import { UserRole } from '../../enums/user-role.enum';

@Entity({ name: 'users' })
export class User extends BaseEntityDate {
	@Column({ nullable: false, length: 16 })
	nick: string;

	@Column({ nullable: false, length: 32 })
	name: string;

	@Column({ nullable: false, length: 32 })
	surname: string;

	@Column({ nullable: false, length: 32, unique: true })
	email: string;

	@Column({ nullable: false, length: 128, select: false })
	@Exclude()
	password: string;

	@Column({
		nullable: false,
		type: 'enum',
		enum: UserRole,
		default: UserRole.JUNIOR,
	})
	role: UserRole;

	@BeforeInsert()
	public encryptPassword(): void {
		this.password = hashSync(this.password, genSaltSync());
	}
}
