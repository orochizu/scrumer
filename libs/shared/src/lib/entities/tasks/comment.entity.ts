import { BaseEntityDate } from '../../abstract/base-entity-date';
import { Column, Entity, ManyToOne } from 'typeorm';
import { Task } from './task.entity';
import { User } from '../auth/user.entity';

@Entity({ name: 'tasks_comments' })
export class Comment extends BaseEntityDate {
	@Column({ nullable: false })
	content: string;

	@ManyToOne(() => Task, (task) => task.id)
	task: Task;

	@ManyToOne(() => User, (user) => user.id)
	owner: User;
}
