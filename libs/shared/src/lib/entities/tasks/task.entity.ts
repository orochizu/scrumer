import { BaseEntityDate } from '../../abstract/base-entity-date';
import { Column, Entity, ManyToOne } from 'typeorm';
import { User } from '../auth/user.entity';
import { TaskPriority } from '../../enums/task-priority.enum';
import { TaskStatus } from '../../enums/task-status.enum';
import { Board } from '../boards/board.entity';

@Entity({ name: 'tasks' })
export class Task extends BaseEntityDate {
	@Column({ nullable: false, length: 32 })
	title: string;

	@Column({ nullable: false })
	desc: string;

	@Column({ nullable: false, type: 'enum', enum: TaskStatus })
	stat: TaskStatus;

	@Column({ nullable: false, type: 'enum', enum: TaskPriority })
	prio: TaskPriority;

	@ManyToOne(() => User, (user) => user.id)
	assignee: User;

	@ManyToOne(() => User, (user) => user.id)
	reporter: User;

	@ManyToOne(() => Board, (board) => board.id)
	board: Board;
}
