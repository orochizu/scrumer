import {
	Column,
	Entity,
	JoinTable,
	ManyToMany,
	ManyToOne,
} from 'typeorm';
import { BaseEntityDate } from '../../abstract/base-entity-date';
import { User } from '../auth/user.entity';

@Entity({ name: 'boards' })
export class Board extends BaseEntityDate {
	@Column({ nullable: false, length: 16 })
	name: string;

	@ManyToOne(() => User, (user) => user.id)
	owner: User;

	@ManyToMany(() => User)
	@JoinTable()
	members: User[];
}
