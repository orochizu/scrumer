import { CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { BaseEntity } from './base-entity';

export abstract class BaseEntityDate extends BaseEntity {
	@CreateDateColumn({ nullable: false })
	created?: Date;

	@UpdateDateColumn({ nullable: false })
	updated?: Date;
}
