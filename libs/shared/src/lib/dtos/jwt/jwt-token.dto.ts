import { IsNotEmpty, IsNumber } from 'class-validator';

export class JwtTokenDto {
	@IsNumber()
	expiresIn: number;

	@IsNumber()
	timestamp: number;

	@IsNotEmpty()
	token: string;
}
