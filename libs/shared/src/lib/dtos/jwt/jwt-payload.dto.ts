import { IsEmail, IsNumber } from 'class-validator';

export class JwtPayloadDto {
	@IsEmail()
	email: string;
	@IsNumber()
	iat: number;
	@IsNumber()
	exp: number;
}
