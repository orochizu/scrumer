import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { PaginationDto } from '../http/pagination.dto';

export class BoardFiltersDto extends PaginationDto {
	@IsString()
	@IsNotEmpty()
	@IsOptional()
	name?: string;

	@IsNumber()
	@IsOptional()
	ownerId?: number;

	@IsNumber()
	@IsOptional()
	memberId?: number;
}
