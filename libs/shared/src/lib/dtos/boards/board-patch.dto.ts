import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class BoardPatchDto {
	@IsString()
	@IsNotEmpty()
	@IsOptional()
	name: string;

	@IsNumber({}, { each: true })
	@IsOptional()
	membersIds: number[];
}
