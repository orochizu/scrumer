import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { UserDto } from '../../..';

export class BoardCreateDto {
	@IsString()
	@IsNotEmpty()
	name: string;

	@IsNumber()
	@IsOptional()
	ownerId?: number;

	@IsNumber({}, { each: true })
	membersIds: number[];
}
