import { TaskPriority } from '../../enums/task-priority.enum';
import { TaskStatus } from '../../enums/task-status.enum';
import {
	IsEmail,
	IsEnum,
	IsNotEmpty,
	IsNumber,
	IsOptional,
	IsString,
} from 'class-validator';

export class TaskCreateDto {
	@IsString()
	@IsNotEmpty()
	name: string;

	@IsString()
	desc: string;

	@IsEnum(TaskStatus)
	stat: TaskStatus;

	@IsEnum(TaskPriority)
	prio: TaskPriority;

	@IsNumber()
	boardId: number;

	@IsEmail()
	@IsNotEmpty()
	@IsOptional()
	assigneeEmail?: string;
}
