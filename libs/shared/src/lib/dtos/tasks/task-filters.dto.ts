import { TaskPriority } from '../../enums/task-priority.enum';
import { TaskStatus } from '../../enums/task-status.enum';
import { IsEnumArray } from '../../decorators/is-enum-array.decorator';
import { IsNumber, IsOptional, IsString } from 'class-validator';
import { PaginationDto } from '../http/pagination.dto';

export class TaskFiltersDto extends PaginationDto {
	@IsString()
	@IsOptional()
	title?: string;

	@IsEnumArray(TaskStatus)
	@IsOptional()
	stat?: TaskStatus[];

	@IsEnumArray(TaskPriority)
	@IsOptional()
	prio?: TaskPriority[];

  @IsNumber()
  @IsOptional()
  boardId?: number;
}
