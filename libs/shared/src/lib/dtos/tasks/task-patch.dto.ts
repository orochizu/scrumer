import { TaskPriority } from '../../enums/task-priority.enum';
import { TaskStatus } from '../../enums/task-status.enum';
import {
  IsEmail,
  IsEnum,
  IsNotEmpty, IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class TaskPatchDto {
	@IsString()
	@IsNotEmpty()
	@IsOptional()
	name?: string;

	@IsString()
	@IsOptional()
	desc?: string;

	@IsEnum(TaskStatus)
	@IsOptional()
	stat?: TaskStatus;

	@IsEnum(TaskPriority)
	@IsOptional()
	prio?: TaskPriority;

	@IsEmail()
	@IsNotEmpty()
	@IsOptional()
	assigneeEmail?: string;

  @IsNumber()
  @IsOptional()
  boardId?: number;
}
