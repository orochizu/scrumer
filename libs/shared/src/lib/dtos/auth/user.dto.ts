import {
	IsAlpha,
	IsAlphanumeric,
	IsEmail,
	IsNotEmpty,
	IsNumber,
	IsOptional,
	IsString,
	MaxLength,
} from 'class-validator';

export class UserDto {
	@IsNumber()
	@IsOptional()
	id?: number;

	@IsString()
	@IsAlphanumeric()
	@MaxLength(16)
	nick: string;

	@IsString()
	@IsAlpha()
	@MaxLength(32)
	name: string;

	@IsString()
	@IsAlpha()
	@MaxLength(32)
	surname: string;

	@IsEmail()
	email: string;

	@IsNotEmpty()
	password: string;
}
