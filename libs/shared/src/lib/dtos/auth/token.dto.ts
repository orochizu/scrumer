import { IsNotEmpty, IsString } from 'class-validator';

export class TokenDto {
	@IsString()
	@IsNotEmpty()
	uuid: string;
}
