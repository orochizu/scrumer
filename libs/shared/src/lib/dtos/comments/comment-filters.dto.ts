import { PaginationDto } from '../http/pagination.dto';
import {
	IsNumberString,
	IsOptional,
	IsString,
} from 'class-validator';

export class CommentFiltersDto extends PaginationDto {
	@IsString()
	@IsOptional()
	content?: string;

	@IsOptional()
	@IsNumberString()
	taskId?: number;

	@IsOptional()
	@IsNumberString()
	ownerId?: number;
}
