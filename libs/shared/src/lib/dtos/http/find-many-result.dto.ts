import { IsArray, IsNumber } from 'class-validator';

export class FindManyResultDto<T> {
	@IsArray()
	results: T[];

	@IsNumber()
	totalCount: number;
}
