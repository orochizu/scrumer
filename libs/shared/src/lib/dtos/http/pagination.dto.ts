import { IsNumberString } from 'class-validator';

export class PaginationDto {
	@IsNumberString()
	skip: number;

	@IsNumberString()
	take: number;
}
