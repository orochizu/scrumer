import { Component } from '@angular/core';

@Component({
	selector: 'scrumer-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent {}
