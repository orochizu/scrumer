import { Action, ActionReducer } from '@ngrx/store';

export type LoadingState = 'loading' | 'resting';

export interface ErrorState {
	error: any;
}

export type CallStateUnion = LoadingState | ErrorState;

export interface CallState {
	callState: {
		single: CallStateUnion;
		batch: CallStateUnion;
	};
}

export interface Triggers {
	loading: string[];
	resting: string[];
	error: string[];
}

export const initialCallState: CallState = {
	callState: {
		single: 'resting',
		batch: 'resting',
	},
};

function extractCallState(
	triggers: Triggers,
	action: Action
): CallStateUnion | null {
	if (!triggers) {
		return null;
	}

	switch (true) {
		case triggers.loading.includes(action.type):
			return 'loading';
		case triggers.resting.includes(action.type):
			return 'resting';
		case triggers.error.includes(action.type):
			return (<{ error?: any }>action).error;
		default:
			return null;
	}
}

export function callStateReducer<S extends CallState, A extends Action>(
	baseReducer: ActionReducer<S, A>,
	{ single, batch }: { single?: Triggers; batch?: Triggers }
) {
	return (state: S, action: A) => {
		const singleCallState = extractCallState(single, action);
		const batchCallState = extractCallState(batch, action);

		return baseReducer(
			singleCallState || batchCallState
				? {
						...state,
						callState: {
							single: singleCallState || state.callState.single,
							batch: batchCallState || state.callState.batch,
						},
				  }
				: state,
			action
		);
	};
}
