import { createSelector, MemoizedSelector } from '@ngrx/store';

import { CallState, CallStateUnion } from './call-state.reducer';

function getCallStateError(callState: CallStateUnion) {
	if (typeof callState === 'string') {
		return null;
	}

	return callState.error;
}

export function getCallStateSelectors<T extends CallState>(
	featureSelector: MemoizedSelector<object, T>
) {
	const getCallState = createSelector(
		featureSelector,
		(featureState) => featureState.callState
	);

	const isSingleLoading = createSelector(
		getCallState,
		(callState) => callState.single === 'loading'
	);

	const isSingleResting = createSelector(
		getCallState,
		(callState) => callState.single === 'resting'
	);

	const getSingleError = createSelector(
		getCallState,
		(callState) => getCallStateError(callState.single)
	);

	const isBatchLoading = createSelector(
		getCallState,
		(callState) => callState.batch === 'loading'
	);

	const isBatchResting = createSelector(
		getCallState,
		(callState) => callState.batch === 'resting'
	);

	const getBatchError = createSelector(
		getCallState,
		(callState) => getCallStateError(callState.batch)
	);

	return {
		isSingleLoading,
		isSingleResting,
		getSingleError,
		isBatchLoading,
		isBatchResting,
		getBatchError,
	};
}
