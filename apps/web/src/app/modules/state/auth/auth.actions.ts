import { Action } from '@ngrx/store';
import {
	EmailDto,
	JwtTokenDto,
	TokenDto,
	User,
	UserDto,
	UserSigninDto,
} from '@scrumer/shared';
import { ErrorState } from '../call-state/call-state.reducer';

export enum AuthActionTypes {
	SignIn = '[AUTH] SignIn',
	SignInSuccess = '[AUTH] SignInSuccess',
	SignInFailure = '[AUTH] SignInFailure',

	SignOut = '[AUTH] SignOut',

	SendToken = '[AUTH] SendToken',
	SendTokenSuccess = '[AUTH] SendTokenSuccess',
	SendTokenFailure = '[AUTH] SendTokenFailure',

	SignUp = '[AUTH] SignUp',
	SignUpSuccess = '[AUTH] SignUpSuccess',
	SignUpFailure = '[AUTH] SignUpFailure',

	GetCurrentUser = '[AUTH] GetCurrentUser',
	GetCurrentUserSuccess = '[AUTH] GetCurrentUserSuccess',
}

// REQUESTS

export class SignIn implements Action {
	readonly type = AuthActionTypes.SignIn;

	constructor(public readonly payload: UserSigninDto) {}
}

export class SignOut implements Action {
	readonly type = AuthActionTypes.SignOut;
}

export class SendToken implements Action {
	readonly type = AuthActionTypes.SendToken;

	constructor(public readonly payload: EmailDto) {}
}

export class SignUp implements Action {
	readonly type = AuthActionTypes.SignUp;

	constructor(public readonly payload: { user: UserDto; token: TokenDto }) {}
}

export class GetCurrentUser implements Action {
	readonly type = AuthActionTypes.GetCurrentUser;
}

// SUCCESSES

export class SignInSuccess implements Action {
	readonly type = AuthActionTypes.SignInSuccess;

	constructor(public readonly payload: JwtTokenDto) {}
}

export class SendTokenSuccess implements Action {
	readonly type = AuthActionTypes.SendTokenSuccess;
}

export class SignUpSuccess implements Action {
	readonly type = AuthActionTypes.SignUpSuccess;
}

export class GetCurrentUserSuccess implements Action {
	readonly type = AuthActionTypes.GetCurrentUserSuccess;

	constructor(public readonly payload: User) {}
}

// FAILURES

export class SignInFailure implements Action {
	readonly type = AuthActionTypes.SignInFailure;

	constructor(public readonly error: ErrorState) {}
}

export class SendTokenFailure implements Action {
	readonly type = AuthActionTypes.SendTokenFailure;

	constructor(public readonly error: ErrorState) {}
}

export class SignUpFailure implements Action {
	readonly type = AuthActionTypes.SignUpFailure;

	constructor(public readonly error: ErrorState) {}
}

export type AuthActions =
	| SignIn
	| SignInSuccess
	| SignInFailure
	| SignOut
	| SendToken
	| SendTokenSuccess
	| SendTokenFailure
	| SignUp
	| SignUpSuccess
	| SignUpFailure
	| GetCurrentUser
	| GetCurrentUserSuccess;
