import { JwtTokenDto, User } from '@scrumer/shared';
import { AuthActions, AuthActionTypes } from './auth.actions';
import {
	CallState,
	callStateReducer,
	initialCallState,
} from '../call-state/call-state.reducer';

export interface AuthState extends CallState {
	me: User;
	token: JwtTokenDto;
	isAuthorized: boolean;
}

export const initialState: AuthState = {
	me: null,
	token: null,
	isAuthorized: false,
	...initialCallState,
};

function authBaseReducer(
	state: AuthState = initialState,
	action: AuthActions
): AuthState {
	switch (action.type) {
		case AuthActionTypes.SignInSuccess:
			return {
				...state,
				token: action.payload,
				isAuthorized: true,
			};
		case AuthActionTypes.GetCurrentUserSuccess:
			return {
				...state,
				me: action.payload,
			};
		case AuthActionTypes.SignOut:
			return { ...initialState };
		default:
			return state;
	}
}

const authCallStateTriggers = {
	single: {
		loading: [
			AuthActionTypes.SendToken,
			AuthActionTypes.SignIn,
			AuthActionTypes.SignUp,
			AuthActionTypes.GetCurrentUser,
		],
		resting: [
			AuthActionTypes.SendTokenSuccess,
			AuthActionTypes.SignInSuccess,
			AuthActionTypes.SignUpSuccess,
			AuthActionTypes.GetCurrentUserSuccess,
		],
		error: [
			AuthActionTypes.SendTokenFailure,
			AuthActionTypes.SignInFailure,
			AuthActionTypes.SignUpFailure,
		],
	},
};

export function authReducer(
	state: AuthState = initialState,
	action: AuthActions
) {
	return callStateReducer(authBaseReducer, authCallStateTriggers)(
		state,
		action
	);
}
