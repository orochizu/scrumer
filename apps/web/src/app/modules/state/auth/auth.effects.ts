import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';

import { AuthState } from './auth.reducer';
import {
	AuthActionTypes,
	GetCurrentUser,
	GetCurrentUserSuccess,
	SendToken,
	SendTokenFailure,
	SendTokenSuccess,
	SignIn,
	SignInFailure,
	SignInSuccess,
	SignOut,
	SignUp,
	SignUpFailure,
	SignUpSuccess,
} from './auth.actions';
import { AuthService } from './auth.service';

import { JwtTokenDto } from '@scrumer/shared';

@Injectable()
export class AuthEffects {
	@Effect() signIn$ = this.dataPersistence.fetch<SignIn>(
		AuthActionTypes.SignIn,
		{
			run: (action: SignIn): Observable<Action> | Action | void => {
				return this.authService
					.signIn(action.payload)
					.pipe(map((res: JwtTokenDto) => new SignInSuccess(res)));
			},
			onError: (
				action: Action,
				error: HttpErrorResponse
			): Observable<Action> | Action => {
				return new SignInFailure(error);
			},
		}
	);

	@Effect() signedIn$ = this.actions$.pipe(
		ofType(AuthActionTypes.SignInSuccess),
		tap(() => this.router.navigate(['/'])),
		map(() => new GetCurrentUser())
	);

	@Effect() signUp$ = this.dataPersistence.fetch<SignUp>(
		AuthActionTypes.SignUp,
		{
			run: (action: SignUp): Observable<Action> | Action | void => {
				return this.authService
					.signUp(action.payload)
					.pipe(map(() => new SignUpSuccess()));
			},
			onError: (
				action: Action,
				error: HttpErrorResponse
			): Observable<any> | any => {
				return new SignUpFailure(error);
			},
		}
	);

	@Effect({ dispatch: false }) signedUp$ = this.actions$.pipe(
		ofType(AuthActionTypes.SignUpSuccess),
		tap(() => this.router.navigate(['/auth']))
	);

	@Effect() sendToken$ = this.dataPersistence.fetch<SendToken>(
		AuthActionTypes.SendToken,
		{
			run: (action: SendToken): Observable<Action> | Action | void => {
				return this.authService
					.sendToken(action.payload)
					.pipe(map(() => new SendTokenSuccess()));
			},
			onError: (
				action: Action,
				error: HttpErrorResponse
			): Observable<Action> | Action => {
				return new SendTokenFailure(error);
			},
		}
	);

	@Effect() getCurrentUser$ = this.dataPersistence.pessimisticUpdate<
		GetCurrentUser
	>(AuthActionTypes.GetCurrentUser, {
		run: (
			action: GetCurrentUser,
			state?: any
		): Observable<Action> | Action | void => {
			const token = state.auth.token;

			return this.authService
				.currentUser(token)
				.pipe(map((user) => new GetCurrentUserSuccess(user)));
		},
		onError: (): Observable<any> | any => {
			return new SignOut();
		},
	});

	constructor(
		private readonly actions$: Actions,
		private readonly dataPersistence: DataPersistence<AuthState>,
		private readonly authService: AuthService,
		private readonly router: Router
	) {}
}
