import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
	EmailDto,
	JwtTokenDto,
	TokenDto,
	User,
	UserDto,
	UserSigninDto,
} from '@scrumer/shared';
import { Observable } from 'rxjs';
import { urlsApi } from '@core/urls/api/urls.api';
import { HttpService } from '@core/services/http.service';

@Injectable()
export class AuthService {
	constructor(
		private readonly httpClient: HttpClient,
		private readonly httpService: HttpService
	) {}

	signIn(credentials: UserSigninDto): Observable<JwtTokenDto> {
		return this.httpClient.post<JwtTokenDto>(urlsApi.auth.signIn, credentials, {
      headers: this.httpService.defaultHeader,
		});
	}

	signUp(userData: { user: UserDto; token: TokenDto }): Observable<UserDto> {
		return this.httpClient.post<UserDto>(urlsApi.auth.signUp, userData, {
      headers: this.httpService.defaultHeader,
		});
	}

	sendToken(recipient: EmailDto): Observable<void> {
		return this.httpClient.post<void>(urlsApi.auth.sendToken, recipient, {
			headers: this.httpService.defaultHeader,
		});
	}

	currentUser(token: JwtTokenDto): Observable<User> {
		return this.httpClient.get<User>(urlsApi.users.me, {
			headers: this.httpService.getAuthHeader(token.token),
		});
	}
}
