import { createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromAuth from './auth.reducer';
import { getCallStateSelectors } from '../call-state/call-state.selectors';

export const getAuthState = createFeatureSelector<fromAuth.AuthState>('auth');

const {
	isSingleLoading,
	isSingleResting,
	getSingleError,
} = getCallStateSelectors(getAuthState);

const getCurrentUser = createSelector(
	getAuthState,
	(state: fromAuth.AuthState) => state.me
);

const getAuthToken = createSelector(
	getAuthState,
	(state: fromAuth.AuthState) => state.token
);

const isAuthorized = createSelector(
	getAuthState,
	(state: fromAuth.AuthState) => state.isAuthorized
);

export const authQuery = {
	isSingleLoading,
	isSingleResting,
	isAuthorized,
	getSingleError,
	getCurrentUser,
	getAuthToken,
};
