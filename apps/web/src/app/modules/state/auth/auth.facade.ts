import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';

import { EmailDto, TokenDto, UserDto, UserSigninDto } from '@scrumer/shared';

import { AuthState } from './auth.reducer';
import { authQuery } from './auth.selectors';
import { SendToken, SignIn, SignOut, SignUp } from './auth.actions';

@Injectable({
	providedIn: 'root',
})
export class AuthFacade {
	isSingleLoading$ = this.store.pipe(select(authQuery.isSingleLoading));
	isSingleResting$ = this.store.pipe(select(authQuery.isSingleResting));
	isAuthorized$ = this.store.pipe(select(authQuery.isAuthorized));
	getSingleError$ = this.store.pipe(select(authQuery.getSingleError));
	getCurrentUser$ = this.store.pipe(select(authQuery.getCurrentUser));
	getAuthToken$ = this.store.pipe(select(authQuery.getAuthToken));

	constructor(private readonly store: Store<AuthState>) {}

	signIn(credentials: UserSigninDto): void {
		this.store.dispatch(new SignIn(credentials));
	}

	signUp(user: UserDto, token: TokenDto): void {
		this.store.dispatch(new SignUp({ user, token }));
	}

	signOut(): void {
		this.store.dispatch(new SignOut());
	}

	verifyEmail(email: EmailDto): void {
		this.store.dispatch(new SendToken(email));
	}
}
