import { Action, ActionReducer, INIT } from '@ngrx/store';
import pick from 'lodash-es/pick';

const getSavedState = (key: string): any => {
	return JSON.parse(localStorage.getItem(key));
};

const setSavedState = (key: string, state: any): void => {
	localStorage.setItem(key, JSON.stringify(state));
};

function localStorageSync(states: string[], statePropsToSkip: string[]) {
	return <S, A extends Action>(reducer: ActionReducer<S, A>) => (
		state: S,
		action: A
	) => {
		const nextState = reducer(state, action);

		if (action.type !== INIT) {
			states.forEach((key) => {
				const propertiesToSave = Object.keys(nextState[key]).filter(
					(property) => !statePropsToSkip.includes(property)
				);
				const stateToSave = pick(nextState[key], propertiesToSave);

				setSavedState(key, stateToSave);
			});
		} else {
			states.forEach((key) => {
				const savedState = getSavedState(key);
				nextState[key] = savedState
					? { ...nextState[key], ...savedState }
					: nextState[key];
			});
		}
		return nextState;
	};
}

export function localStorageSyncReducer<S, A extends Action>(
	reducer: ActionReducer<S, A>
) {
	return localStorageSync(['auth'], ['callState'])(reducer);
}
