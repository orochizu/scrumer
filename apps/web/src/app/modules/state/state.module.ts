import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { DataPersistence, NxModule } from '@nrwl/angular';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { metaReducers, reducers } from './';

import { environment } from '../../../environments/environment';
import { AuthService } from './auth/auth.service';
import { AuthEffects } from './auth/auth.effects';
import { AuthFacade } from './auth/auth.facade';

@NgModule({
	declarations: [],
	imports: [
		NxModule,
		StoreModule.forRoot(reducers, { metaReducers }),
		EffectsModule.forRoot([AuthEffects]),
		environment.production
			? []
			: StoreDevtoolsModule.instrument({ maxAge: 10 }),
	],
	providers: [AuthService, AuthEffects, AuthFacade, DataPersistence],
})
export class StateModule {}
