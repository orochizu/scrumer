import * as fromAuth from './auth/auth.reducer';
import * as fromLocalStorage from './local-storage/local-storage.reducer';
import { ActionReducerMap, MetaReducer } from '@ngrx/store';

export interface AppState {
	auth: fromAuth.AuthState;
}

export const reducers: ActionReducerMap<AppState> = {
	auth: fromAuth.authReducer,
};

export const metaReducers: MetaReducer<any, any>[] = [
	fromLocalStorage.localStorageSyncReducer,
];
