import { Directive, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
	selector: '[scrumerDisableControl]',
})
export class DisableControlDirective {
	@Input() set scrumerDisableControl(condition: boolean) {
		const action = condition ? 'disable' : 'enable';
		this.ngControl.control[action]();
	}

	constructor(private readonly ngControl: NgControl) {}
}
