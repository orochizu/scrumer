import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
	selector: '[scrumerToggle]',
})
export class ToggleDirective {
	@Input('scrumerToggle') associatedEls: HTMLElement[] = [];

	constructor(private el: ElementRef) {}

	@HostListener('click') onClick(): void {
		this.el.nativeElement.classList.toggle('is-active');
		this.associatedEls.forEach((el) => el.classList.toggle('is-active'));
	}

	@HostListener('document:click', ['$event.target']) onDOMClick(target): void {
		const clickedOutside = !this.el.nativeElement.contains(target);

		if (clickedOutside) {
			this.el.nativeElement.classList.remove('is-active');
			this.associatedEls.forEach((el) => el.classList.remove('is-active'));
		}
	}
}
