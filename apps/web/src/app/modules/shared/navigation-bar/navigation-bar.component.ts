import {
	ChangeDetectionStrategy,
	Component,
} from '@angular/core';

import { Observable } from 'rxjs';

import { AuthFacade } from '@modules/state/auth/auth.facade';

import { User } from '@scrumer/shared';

@Component({
	selector: 'scrumer-navigation-bar',
	templateUrl: './navigation-bar.component.html',
	styleUrls: ['./navigation-bar.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavigationBarComponent {
	public isAuthorized$: Observable<boolean> = this.auth.isAuthorized$;
	public currentUser$: Observable<User> = this.auth.getCurrentUser$;

	constructor(private auth: AuthFacade) {}
}
