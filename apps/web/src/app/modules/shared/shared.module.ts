import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { TextInputComponent } from './text-input/text-input.component';
import { DisableControlDirective } from './directives/disable-control.directive';
import { ToggleDirective } from './directives/toggle.directive';

const MODULES = [
	CommonModule,
	FontAwesomeModule,
	FormsModule,
	ReactiveFormsModule,
	RouterModule,
];

const DECLARATIONS = [
	NavigationBarComponent,
	TextInputComponent,
	DisableControlDirective,
	ToggleDirective,
];

@NgModule({
	declarations: [...DECLARATIONS],
	imports: [...MODULES],
	exports: [...MODULES, ...DECLARATIONS],
})
export class SharedModule {}
