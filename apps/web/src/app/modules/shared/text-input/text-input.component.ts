import {
	Component,
	ChangeDetectionStrategy,
	Input,
	forwardRef,
	ChangeDetectorRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { IconProp } from '@fortawesome/fontawesome-svg-core';

type InputType = 'text' | 'email' | 'password' | 'tel';

@Component({
	selector: 'scrumer-text-input',
	templateUrl: './text-input.component.html',
	styleUrls: ['./text-input.component.scss'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => TextInputComponent),
			multi: true,
		},
	],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextInputComponent implements ControlValueAccessor {
	@Input() type?: InputType = 'text';

	@Input() val: string = '';
	@Input() label?: string;
	@Input() placeholder?: string;

	@Input() loading?: boolean;
	@Input() invalid?: boolean;
	@Input() disabled?: boolean;

	@Input() endIcon?: IconProp;
	@Input() startIcon?: IconProp;

	onChange: any = (_: any) => {};
	onTouched: any = () => {};

	constructor(private detector: ChangeDetectorRef) {}

	writeValue(val: string): void {
		this.value = val;
	}

	setDisabledState(isDisabled: boolean): void {
		this.disabled = isDisabled;
		this.detector.markForCheck();
	}

	registerOnChange(fn: any): void {
		this.onChange = fn;
	}

	registerOnTouched(fn: any): void {
		this.onTouched = fn;
	}

	get value() {
		return this.val;
	}

	set value(val) {
		this.val = val;
		this.onChange(val);
		this.detector.markForCheck();
	}
}
