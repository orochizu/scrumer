import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthViewGuard } from '@core/guards/auth-view.guard';

const routes: Routes = [
	{
		path: 'auth',
		canLoad: [AuthViewGuard],
		loadChildren: () =>
			import('./views/auth/auth.module').then((m) => m.AuthModule),
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
