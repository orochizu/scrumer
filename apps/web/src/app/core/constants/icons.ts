import { faEnvelope } from '@fortawesome/free-solid-svg-icons/faEnvelope';
import { faLock } from '@fortawesome/free-solid-svg-icons/faLock';
import { faKey } from '@fortawesome/free-solid-svg-icons/faKey';
import { faUser } from '@fortawesome/free-solid-svg-icons/faUser';
import { faUserSecret } from '@fortawesome/free-solid-svg-icons/faUserSecret';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

export interface Icons {
	faEnvelope: IconProp;
	faLock: IconProp;
	faKey: IconProp;
	faUser: IconProp
  faUserSecret: IconProp
}

export const icons: Icons = {
	faEnvelope,
	faLock,
	faKey,
  faUser,
  faUserSecret
};
