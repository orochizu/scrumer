import { OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

export abstract class BaseSubscriber implements OnDestroy {
	subscriptions: Subscription[];

	registerSubscription(...subscriptions: Subscription[]): void {
		subscriptions.forEach((sub) => {
			if (sub.unsubscribe) {
				this.subscriptions.push(sub);
			}
		});
	}

	ngOnDestroy(): void {
		this.unsubscribe();
	}

	unsubscribe(): void {
		this.subscriptions.forEach((sub) => {
			if (sub.unsubscribe) {
				sub.unsubscribe();
			} else {
				throw new TypeError('Cannot unsubscribe from not Rx subscription');
			}
		});
	}
}
