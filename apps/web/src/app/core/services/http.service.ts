import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GenericObject } from '@scrumer/shared';

@Injectable({
	providedIn: 'root',
})
export class HttpService {
	private readonly _defaultHeader: GenericObject = {
		'Content-Type': 'application/json',
	};

	constructor(private readonly http: HttpClient) {}

	get defaultHeader(): GenericObject {
		return this._defaultHeader;
	}

	getAuthHeader(token: string): GenericObject {
		return { ...this.defaultHeader, Authorization: `Bearer ${token}` };
	}
}
