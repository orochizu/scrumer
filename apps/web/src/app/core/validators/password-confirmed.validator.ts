import { FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';

export const passwordConfirmedValidator: ValidatorFn = (
	control: FormGroup
): ValidationErrors | null => {
	const password = control.get('password');
	const confirmedPassword = control.get('confirmedPassword');

	return password && confirmedPassword && password.value !== confirmedPassword.value ? { passwordConfirmed: true } : null
};
