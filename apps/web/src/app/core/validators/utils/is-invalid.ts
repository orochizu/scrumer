import { AbstractControl } from '@angular/forms';

export function isInvalid(field: AbstractControl): boolean {
	return field.invalid && (field.touched && field.dirty);
}
