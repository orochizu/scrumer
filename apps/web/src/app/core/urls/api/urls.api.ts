import { environment } from '../../../../environments/environment';

interface AuthEndpoints {
	signIn: string;
	signUp: string;
	sendToken: string;
	validateToken: string;
}

interface UsersEndpoints {
	me: string;
}

const { apiHost, apiPrefix } = environment;

const AUTH_PREFIX = `${apiHost}/${apiPrefix}/auth`;

const USERS_PREFIX = `${apiHost}/${apiPrefix}/users`;

const authEndpoints: AuthEndpoints = {
	signIn: `${AUTH_PREFIX}/signin`,
	signUp: `${AUTH_PREFIX}/signup`,
	sendToken: `${AUTH_PREFIX}/sendToken`,
	validateToken: `${AUTH_PREFIX}/validateToken`,
};

const usersEndpoints: UsersEndpoints = {
	me: `${USERS_PREFIX}/me`,
};

export class UrlsApi {
	public readonly auth = authEndpoints;
	public readonly users = usersEndpoints;
}

export const urlsApi = new UrlsApi();
