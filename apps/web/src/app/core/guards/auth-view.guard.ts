import { Injectable } from '@angular/core';
import {
	CanLoad,
	Route,
	UrlSegment,
} from '@angular/router';

import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { AuthFacade } from '@modules/state/auth/auth.facade';

@Injectable({
	providedIn: 'root',
})
export class AuthViewGuard implements CanLoad {
	constructor(
		private readonly auth: AuthFacade,
	) {}

	canLoad(
		route: Route,
		segments: UrlSegment[]
	): Observable<boolean> | Promise<boolean> | boolean {
		return this.auth.isAuthorized$.pipe(
			map((isAuthorized) => !isAuthorized),
			take(1)
		);
	}
}
