import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
	AbstractControl,
	FormBuilder,
	FormGroup,
	Validators,
} from '@angular/forms';

import { Observable } from 'rxjs';

import { icons } from '@core/constants/icons';
import { isInvalid } from '@core/validators/utils/is-invalid';

import { AuthFacade } from '@modules/state/auth/auth.facade';

@Component({
	selector: 'scrumer-sign-in',
	templateUrl: './sign-in.component.html',
	styleUrls: ['./sign-in.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignInComponent {
	public isLoading$: Observable<boolean> = this.auth.isSingleLoading$;
	public signInForm: FormGroup = this.formBuilder.group({
		email: ['', { validators: [Validators.required, Validators.email] }],
		password: [
			'',
			{
				validators: [
					Validators.required,
					Validators.minLength(4),
					Validators.maxLength(32),
				],
			},
		],
	});
	public readonly icons = icons;

	constructor(
		private readonly formBuilder: FormBuilder,
		private readonly auth: AuthFacade
	) {}

	onSubmitSignIn(): void {
		this.auth.signIn(this.signInForm.value);
	}

	isInvalid(field: AbstractControl): boolean {
		return isInvalid(field);
	}

	get email(): AbstractControl {
		return this.signInForm.get('email');
	}

	get password(): AbstractControl {
		return this.signInForm.get('password');
	}
}
