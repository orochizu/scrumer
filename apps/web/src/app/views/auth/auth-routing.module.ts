import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SignInComponent } from './sign-in/sign-in.component';
import { AuthComponent } from './auth.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { VerificationEmailFormComponent } from './sign-up/verification-email-form/verification-email-form.component';
import { CreateAccountFormComponent } from './sign-up/create-account-form/create-account-form.component';

const routes: Routes = [
	{
		path: '',
		component: AuthComponent,
		children: [
			{ path: 'signIn', component: SignInComponent },
			{
				path: 'signUp',
				component: SignUpComponent,
				children: [
					{
						path: '',
						pathMatch: 'full',
						redirectTo: 'verifyEmail',
					},
					{ path: 'verifyEmail', component: VerificationEmailFormComponent },
					{ path: 'createAccount', component: CreateAccountFormComponent },
				],
			},
			{
				path: '',
				pathMatch: 'full',
				redirectTo: 'signIn',
			},
		],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class AuthRoutingModule {}
