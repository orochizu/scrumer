import { NgModule } from '@angular/core';

import { SharedModule } from '@modules/shared/shared.module';

import { AuthComponent } from './auth.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AuthRoutingModule } from './auth-routing.module';
import { VerificationEmailFormComponent } from './sign-up/verification-email-form/verification-email-form.component';
import { CreateAccountFormComponent } from './sign-up/create-account-form/create-account-form.component';

@NgModule({
	declarations: [
		AuthComponent,
		SignInComponent,
		SignUpComponent,
		VerificationEmailFormComponent,
		CreateAccountFormComponent,
	],
	imports: [AuthRoutingModule, SharedModule],
})
export class AuthModule {}
