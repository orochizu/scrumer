import { Component, ChangeDetectionStrategy } from '@angular/core';
import {
	AbstractControl,
	FormBuilder,
	FormGroup,
	Validators,
} from '@angular/forms';

import { Observable } from 'rxjs';

import { icons } from '@core/constants/icons';
import { isInvalid } from '@core/validators/utils/is-invalid';

import { AuthFacade } from '@modules/state/auth/auth.facade';

@Component({
	selector: 'scrumer-verification-email-form',
	templateUrl: './verification-email-form.component.html',
	styleUrls: ['./verification-email-form.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VerificationEmailFormComponent {
	public isLoading$: Observable<boolean> = this.auth.isSingleLoading$;
	public verifyEmailForm: FormGroup = this.formBuilder.group({
		email: ['', { validators: [Validators.required, Validators.email] }],
	});
	public readonly icons = icons;

	constructor(
		private readonly formBuilder: FormBuilder,
		private readonly auth: AuthFacade
	) {}

	get email(): AbstractControl {
		return this.verifyEmailForm.get('email');
	}

	onSubmitVerifyEmail(): void {
		this.auth.verifyEmail({ email: this.email.value });
	}

	isInvalid(field: AbstractControl): boolean {
		return isInvalid(field);
	}
}
