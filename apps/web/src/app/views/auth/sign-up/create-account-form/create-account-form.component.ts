import { Component, ChangeDetectionStrategy } from '@angular/core';
import {
	AbstractControl,
	FormBuilder,
	FormGroup,
	Validators,
} from '@angular/forms';

import { Observable } from 'rxjs';

import { passwordConfirmedValidator } from '@core/validators/password-confirmed.validator';
import { isInvalid } from '@core/validators/utils/is-invalid';
import { icons } from '@core/constants/icons';

import { AuthFacade } from '@modules/state/auth/auth.facade';

@Component({
	selector: 'scrumer-create-user-form',
	templateUrl: './create-account-form.component.html',
	styleUrls: ['./create-account-form.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateAccountFormComponent {
	public isLoading$: Observable<boolean> = this.auth.isSingleLoading$;
	public createAccountForm: FormGroup = this.formBuilder.group({
		token: this.formBuilder.group({
			uuid: [
				'',
				{
					validators: [
						Validators.required,
						Validators.minLength(36),
						Validators.maxLength(36),
					],
				},
			],
		}),
		user: this.formBuilder.group(
			{
				nick: [
					'',
					{
						validators: [
							Validators.required,
							Validators.minLength(2),
							Validators.maxLength(16),
						],
					},
				],
				name: [
					'',
					{
						validators: [
							Validators.required,
							Validators.minLength(2),
							Validators.maxLength(32),
						],
					},
				],
				surname: [
					'',
					{
						validators: [
							Validators.required,
							Validators.minLength(2),
							Validators.maxLength(32),
						],
					},
				],
				email: [
					'',
					{
						validators: [Validators.required, Validators.email],
					},
				],
				password: [
					'',
					{
						validators: [
							Validators.pattern(
								/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/
							),
						],
					},
				],
				confirmedPassword: [
					'',
					{
						validators: [
							Validators.pattern(
								/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/
							),
						],
					},
				],
			},
			{ validators: passwordConfirmedValidator }
		),
	});
	public readonly icons = icons;

	constructor(
		private readonly formBuilder: FormBuilder,
		private readonly auth: AuthFacade
	) {}

	onSubmitCreateUserForm(): void {
		const { confirmedPassword, ...user } = this.user.value;
		this.auth.signUp({ ...user }, { ...this.token.value });
	}

	isInvalid(field: AbstractControl): boolean {
		return isInvalid(field);
	}

	get token(): FormGroup {
		return this.createAccountForm.get('token') as FormGroup;
	}

	get user(): FormGroup {
		return this.createAccountForm.get('user') as FormGroup;
	}

	get uuid(): AbstractControl {
		return this.token.get('uuid');
	}

	get nick(): AbstractControl {
		return this.user.get('nick');
	}

	get name(): AbstractControl {
		return this.user.get('name');
	}

	get surname(): AbstractControl {
		return this.user.get('surname');
	}

	get email(): AbstractControl {
		return this.user.get('email');
	}

	get password(): AbstractControl {
		return this.user.get('password');
	}

	get confirmedPassword(): AbstractControl {
		return this.user.get('confirmedPassword');
	}
}
