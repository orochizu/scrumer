import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/services/config.service';
import { User } from '@scrumer/shared';
import { Token } from '@scrumer/shared';
import { Task } from '@scrumer/shared';
import { Comment } from '@scrumer/shared';
import { Board } from '@scrumer/shared';

@Module({
	imports: [
		TypeOrmModule.forRootAsync({
			imports: [ConfigModule],
			inject: [ConfigService],
			useFactory: (configs: ConfigService) => {
				return {
					type: 'postgres',
					host: configs.getPostgresHost,
					port: configs.getPostgresPort,
					username: configs.getPostgresUsername,
					password: configs.getPostgresPassword,
					database: configs.getPostgresDatabase,
					entities: [User, Token, Task, Comment, Board],
					synchronize: true,
				};
			},
		}),
	],
})
export class DatabaseModule {}
