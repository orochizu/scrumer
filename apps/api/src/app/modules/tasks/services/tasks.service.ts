import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Like, Repository } from 'typeorm';
import { Task } from '@scrumer/shared';
import { User } from '@scrumer/shared';
import {
	FindManyResultDto,
	TaskCreateDto,
	TaskPatchDto,
	TaskFiltersDto,
} from '@scrumer/shared';
import { UsersService } from '../../users/services/users.service';
import { BoardsService } from '../../boards/services/boards.service';

@Injectable()
export class TasksService {
	constructor(
		@InjectRepository(Task)
		private readonly tasks: Repository<Task>,
		private readonly users: UsersService,
		private readonly boards: BoardsService
	) {}

	async save(reporter: User, taskCreateDto: TaskCreateDto): Promise<Task> {
		const { assigneeEmail, boardId } = taskCreateDto;

		const assignee = assigneeEmail
			? await this.users.findByEmail(assigneeEmail)
			: undefined;

		const board = boardId ? await this.boards.findOneById(boardId) : undefined;

		if (board) {
			throw new BadRequestException('Task must be assigned to board');
		}

		const task = this.tasks.create({
			...taskCreateDto,
			reporter,
			assignee,
		});

		return this.tasks.save(task);
	}

	async updateOneById(id: number, taskPatchDto: TaskPatchDto): Promise<Task> {
		const task = await this.tasks
			.findOne(id)
			.then((t: Task) => this.tasks.create({ ...t, ...taskPatchDto }));
		return this.tasks.save(task);
	}

	delete(id: number | number[]) {
		return this.tasks.delete(id);
	}

	findById(id: number): Promise<Task> {
		return this.tasks.findOne(id);
	}

	findAllByFilters(filters: TaskFiltersDto): Promise<FindManyResultDto<Task>> {
		const { title, stat, prio, skip, boardId, take } = filters;
		return this.tasks
			.findAndCount({
				where: {
					title: Like(`%${title ? title : ''}%`),
					...(stat && stat.length && { stat: In([...stat]) }),
					...(prio && prio.length && { prio: In([...prio]) }),
					...(boardId && { board: { id: boardId } }),
				},
				skip,
				take,
			})
			.then(([results, totalCount]) => ({ results, totalCount }));
	}
}
