import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Equal, In, Like, Repository } from 'typeorm';
import { Comment } from '@scrumer/shared';
import {
	CommentCreateDto,
	CommentFiltersDto,
	FindManyResultDto,
} from '@scrumer/shared';
import { User } from '@scrumer/shared';
import { TasksService } from './tasks.service';
import { isArray } from 'util';

@Injectable()
export class CommentsService {
	constructor(
		@InjectRepository(Comment)
		private readonly comments: Repository<Comment>,
		private readonly tasks: TasksService
	) {}

	async save(
		owner: User,
		commentCreateDto: CommentCreateDto
	): Promise<Comment> {
		const task = await this.tasks.findById(commentCreateDto.taskId);

		if (task) {
			const comment = this.comments.create({
				...commentCreateDto,
				owner,
				task,
			});

			return this.comments.save(comment);
		} else {
			throw new UnprocessableEntityException(
				'Comment have to be assigned to existing task'
			);
		}
	}

	deleteForCurrentUser(
		user: User,
		ids: number | number[]
	): Promise<DeleteResult> {
		return this.comments.delete({
			id: isArray(ids) ? In(ids) : Equal(ids),
			owner: { id: user.id },
		});
	}

	findAllByFilters(
		filters: CommentFiltersDto
	): Promise<FindManyResultDto<Comment>> {
		const { content, taskId, ownerId, skip, take } = filters;
		return this.comments
			.findAndCount({
				where: {
					content: Like(`%${content ? content : ''}%`),
					...(taskId && { task: { id: taskId } }),
					...(ownerId && { owner: { id: ownerId } }),
				},
				skip,
				take,
			})
			.then(([results, totalCount]) => ({ results, totalCount }));
	}
}
