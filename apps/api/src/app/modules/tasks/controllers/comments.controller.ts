import {
	Body,
	Controller,
	Delete,
	Get,
	Post,
	Query,
	UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CommentsService } from '../services/comments.service';
import { CurrentUser } from '../../../core/decorators/current-user.decorator';
import { User } from '@scrumer/shared';
import {
	CommentCreateDto,
	CommentFiltersDto,
	FindManyResultDto,
} from '@scrumer/shared';
import { Comment } from '@scrumer/shared';
import { DeleteResult } from 'typeorm';

@Controller('comments')
@UseGuards(AuthGuard())
export class CommentsController {
	constructor(private readonly comments: CommentsService) {}

	@Post()
	async create(
		@CurrentUser() user: User,
		@Body() comment: CommentCreateDto
	): Promise<Comment> {
		return this.comments.save(user, comment);
	}

	@Delete()
	async deleteAllByIds(
		@CurrentUser() user: User,
		@Query('ids') ids: number[]
	): Promise<DeleteResult> {
		return this.comments.deleteForCurrentUser(user, ids);
	}

	@Get()
	async findAllByFilters(
		@Query() filters: CommentFiltersDto
	): Promise<FindManyResultDto<Comment>> {
		return this.comments.findAllByFilters(filters);
	}
}
