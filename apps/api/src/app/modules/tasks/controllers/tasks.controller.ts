import {
	Body,
	Controller,
	Delete,
	Get,
	Param,
	ParseIntPipe,
	Patch,
	Post,
	Query,
	UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CurrentUser } from '../../../core/decorators/current-user.decorator';
import { User } from '@scrumer/shared';
import {
	FindManyResultDto,
	TaskCreateDto,
	TaskPatchDto,
	TaskFiltersDto,
} from '@scrumer/shared';
import { TasksService } from '../services/tasks.service';
import { Task } from '@scrumer/shared';
import { DeleteResult } from 'typeorm';

@Controller('tasks')
@UseGuards(AuthGuard())
export class TasksController {
	constructor(private readonly tasks: TasksService) {}

	@Post()
	async create(
		@CurrentUser() user: User,
		@Body() task: TaskCreateDto
	): Promise<Task> {
		return this.tasks.save(user, task);
	}

	@Patch(':id')
	async updateOneById(
		@Param('id', new ParseIntPipe()) id: number,
		@Body() task: TaskPatchDto
	): Promise<Task> {
		return this.tasks.updateOneById(id, task);
	}

	@Delete()
	async deleteAllByIds(
		@Query('ids') ids: number[]
	): Promise<DeleteResult> {
		return this.tasks.delete(ids);
	}

	@Get(':id')
	async findOneById(
		@Param('id', new ParseIntPipe()) id: number
	): Promise<Task> {
		return this.tasks.findById(id);
	}

	@Get()
	async findAllByFilters(
		@Query() filters: TaskFiltersDto
	): Promise<FindManyResultDto<Task>> {
		return this.tasks.findAllByFilters(filters);
	}
}
