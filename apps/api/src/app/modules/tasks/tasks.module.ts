import { Module } from '@nestjs/common';
import { TasksController } from './controllers/tasks.controller';
import { TasksService } from './services/tasks.service';
import { ConfigModule } from '../config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { CommentsService } from './services/comments.service';
import { Task } from '@scrumer/shared';
import { Comment } from '@scrumer/shared';
import { CommentsController } from './controllers/comments.controller';
import { UsersModule } from '../users/users.module';
import { BoardsModule } from '../boards/boards.module';

@Module({
	imports: [
		ConfigModule,
		UsersModule,
		BoardsModule,
		TypeOrmModule.forFeature([Task, Comment]),
		PassportModule.register({ defaultStrategy: 'jwt' }),
	],
	controllers: [TasksController, CommentsController],
	providers: [TasksService, CommentsService],
	exports: [TasksService, CommentsService],
})
export class TasksModule {}
