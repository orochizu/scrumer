import { Module } from '@nestjs/common';
import { ConfigModule } from '../config/config.module';
import { UsersModule } from '../users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { Board } from '@scrumer/shared';
import { BoardsController } from './controllers/boards.controller';
import { BoardsService } from './services/boards.service';

@Module({
	imports: [
		ConfigModule,
		UsersModule,
		TypeOrmModule.forFeature([Board]),
		PassportModule.register({ defaultStrategy: 'jwt' }),
	],
	controllers: [BoardsController],
	providers: [BoardsService],
	exports: [BoardsService],
})
export class BoardsModule {}
