import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Board } from '@scrumer/shared';
import { Repository } from 'typeorm';
import { User } from '@scrumer/shared';
import {
	BoardCreateDto,
	BoardFiltersDto,
	BoardPatchDto,
	FindManyResultDto,
} from '@scrumer/shared';
import { UsersService } from '../../users/services/users.service';

@Injectable()
export class BoardsService {
	constructor(
		@InjectRepository(Board)
		private readonly boards: Repository<Board>,
		private readonly users: UsersService
	) {}

	async save(owner: User, boardCreateDto: BoardCreateDto): Promise<Board> {
		const members = await this.users.findByIds(boardCreateDto.membersIds);
		const board = this.boards.create({ ...boardCreateDto, members, owner });

		return this.boards.save(board);
	}

	async updateOneById(
		id: number,
		boardPatchDto: BoardPatchDto
	): Promise<Board> {
		const { membersIds } = boardPatchDto;
		const oldBoard = await this.boards.findOne(id, { relations: ['members'] });
		const members = membersIds
			? await this.users.findByIds(membersIds)
			: oldBoard.members;

		const board = this.boards.create({
			...oldBoard,
			...boardPatchDto,
			members,
		});

		return this.boards.save(board);
	}

	findOneById(id: number): Promise<Board> {
		return this.boards.findOne(id, { relations: ['owner', 'members'] });
	}

	findAllByFilters(
		filters: BoardFiltersDto
	): Promise<FindManyResultDto<Board>> {
		const { name, ownerId, memberId, skip, take } = filters;

		let boardsQuery = this.boards
			.createQueryBuilder('board')
			.skip(skip)
			.take(take);

		if (name) {
			boardsQuery = boardsQuery.where('board.name LIKE :name', {
				name: `%${name}%`,
			});
		}

		if (ownerId) {
			boardsQuery = boardsQuery
				.leftJoinAndSelect('board.owner', 'owner')
				.andWhere('owner.id = :ownerId', { ownerId });
		}

		if (memberId) {
			boardsQuery = boardsQuery
				.leftJoinAndSelect('board.members', 'member')
				.andWhere('member.id = :memberId', { memberId });
		}

		return boardsQuery
			.getManyAndCount()
			.then(([results, totalCount]) => ({ results, totalCount }));
	}
}
