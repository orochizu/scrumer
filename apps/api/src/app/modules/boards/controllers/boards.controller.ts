import {
	Body,
	Controller,
	Get,
	Param,
	ParseIntPipe,
	Patch,
	Post,
	Query,
	UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BoardsService } from '../services/boards.service';
import { CurrentUser } from '../../../core/decorators/current-user.decorator';
import { User } from '@scrumer/shared';
import {
	BoardCreateDto,
	BoardPatchDto,
	CommentFiltersDto,
	FindManyResultDto,
} from '@scrumer/shared';
import { Board } from '@scrumer/shared';

@Controller('boards')
@UseGuards(AuthGuard())
export class BoardsController {
	constructor(private readonly boards: BoardsService) {}

	@Post()
	async create(
		@CurrentUser() user: User,
		@Body() board: BoardCreateDto
	): Promise<Board> {
		return this.boards.save(user, board);
	}

	@Patch(':id')
	async updateOneById(
		@Param('id', new ParseIntPipe()) id: number,
		@Body() board: BoardPatchDto
	): Promise<Board> {
		return this.boards.updateOneById(id, board);
	}

	@Get(':id')
	async findOneById(@Param('id', new ParseIntPipe()) id: number) {
		return this.boards.findOneById(id);
	}

	@Get()
	async findAllByFilters(
		@Query() filters: CommentFiltersDto
	): Promise<FindManyResultDto<Board>> {
		return this.boards.findAllByFilters(filters);
	}
}
