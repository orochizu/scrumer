import { Injectable, Logger } from '@nestjs/common';
import { Transporter } from 'nodemailer';
import { ConfigService } from '../../config/services/config.service';
import { createTransport } from 'nodemailer';
import * as Mail from 'nodemailer/lib/mailer';

// TODO: MailerService needs to use PUG mail templates instead of HTML mail templates

@Injectable()
export class MailerService {
	private transporter: Transporter;
	constructor(private readonly configs: ConfigService) {
		this.init();
	}

	public sendMail(options: Mail.Options): void {
		this.transporter
			.sendMail({
				...options,
				from: this.configs.getMailerMailAddress,
			})
			.then((value) => Logger.log(value))
			.catch((error) => Logger.log(error));
	}

	private init(): void {
		this.transporter = createTransport({
			service: this.configs.getMailerMailService,
			auth: {
				user: this.configs.getMailerMailAddress,
				pass: this.configs.getMailerMailPassword,
			},
		});
	}
}
