import * as dotenv from 'dotenv';
import * as Joi from 'joi';
import * as fs from 'fs';
import { GenericObject } from '@scrumer/shared';
import { Injectable, Logger } from '@nestjs/common';

@Injectable()
export class ConfigService {
	private readonly envConfig: GenericObject;

	constructor(filePath: string) {
		this.envConfig = ConfigService.validateInput(
			dotenv.parse(fs.readFileSync(filePath))
		);
		Logger.log(this.envConfig);
	}

	private static validateInput(envConfig: GenericObject): GenericObject {
		const envSchema: Joi.ObjectSchema = Joi.object({
			NODE_ENV: Joi.string().default('develop'),
			POSTGRES_HOST: Joi.string().default('localhost'),
			POSTGRES_PORT: Joi.number().default(5433),
			POSTGRES_USER: Joi.string().default('root'),
			POSTGRES_PASSWORD: Joi.string().default('root'),
			POSTGRES_DB: Joi.string().default('planeo-db'),
			AUTH_SECRET_KEY: Joi.string().default('UCqyyIsDQaPTC1ha'),
			AUTH_TOKEN_EXPIRATION: Joi.number().default(86400),
			MAILER_MAIL_ADDRESS: Joi.string()
				.email()
				.default('planeo.noreply@gmail.com'),
			MAILER_MAIL_PASSWORD: Joi.string(),
			MAILER_MAIL_SERVICE: Joi.string().default('gmail'),
		});

		const { error, value: validatedEnvConfig } = Joi.validate(
			envConfig,
			envSchema
		);

		if (error) {
			throw new Error(`Config validation error: ${error.message}`);
		}

		return validatedEnvConfig;
	}

	get getNodeEnv(): string {
		return this.envConfig.NODE_ENV;
	}

	get getPostgresHost(): string {
		return this.envConfig.POSTGRES_HOST;
	}

	get getPostgresPort(): number {
		return Number(this.envConfig.POSTGRES_PORT);
	}

	get getPostgresUsername(): string {
		return this.envConfig.POSTGRES_USER;
	}

	get getPostgresPassword(): string {
		return this.envConfig.POSTGRES_PASSWORD;
	}

	get getPostgresDatabase(): string {
		return this.envConfig.POSTGRES_DB;
	}

	get getAuthSecretKey(): string {
		return this.envConfig.AUTH_SECRET_KEY;
	}

	get getAuthTokenExpiration(): number {
		return Number(this.envConfig.AUTH_TOKEN_EXPIRATION);
	}

	get getMailerMailAddress(): string {
		return this.envConfig.MAILER_MAIL_ADDRESS;
	}

	get getMailerMailPassword(): string {
		return this.envConfig.MAILER_MAIL_PASSWORD;
	}

	get getMailerMailService(): string {
		return this.envConfig.MAILER_MAIL_SERVICE;
	}
}
