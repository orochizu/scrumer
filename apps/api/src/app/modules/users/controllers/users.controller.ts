import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { User } from '@scrumer/shared';
import { CurrentUser } from '../../../core/decorators/current-user.decorator';

@Controller('users')
@UseGuards(AuthGuard())
export class UsersController {
	constructor() {}

	@Get('me')
	async currentUser(@CurrentUser() user: User): Promise<User> {
		return user;
	}
}
