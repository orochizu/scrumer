import { Test } from '@nestjs/testing';
import { Repository } from 'typeorm';
import { User } from '@scrumer/shared';
import { UsersService } from './users.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UserDto, UserRole } from '@scrumer/shared';
import { genSaltSync, hashSync } from 'bcrypt';
import { ConflictException, HttpStatus } from '@nestjs/common';

const mockUserDto: UserDto = {
	nick: 'TestNick',
	name: 'TestName',
	surname: 'TestSurname',
	role: UserRole.REGULAR,
	email: 'test@scrumer.com',
	password: hashSync('TestPassword', genSaltSync()),
};

const mockUser = {
	id: 1,
	created: new Date(),
	updated: new Date(),
	...mockUserDto,
};

const getMockUsersRepository = () => ({
	create: jest.fn().mockReturnValue(mockUser),
});

describe('UsersService', () => {
	let usersService: UsersService;
	let usersRepository: Repository<User>;

	beforeEach(async () => {
		const module = await Test.createTestingModule({
			providers: [
				UsersService,
				{
					provide: getRepositoryToken(User),
					useFactory: getMockUsersRepository,
				},
			],
		}).compile();

		usersService = module.get<UsersService>(UsersService);
		usersRepository = module.get(getRepositoryToken(User));
	});

	describe('Save user', () => {
		it('should save and return user', async () => {
			usersRepository.save = jest.fn().mockResolvedValue(mockUser);

			usersService.save(mockUserDto).then((result) => {
				expect(result).toEqual(mockUser);
			});
		});

		it('should throw ConflictException', async () => {
			usersRepository.save = jest
				.fn()
				.mockRejectedValue(new ConflictException());

			usersService.save(mockUserDto).catch((err) => {
				expect(err.status).toEqual(HttpStatus.CONFLICT);
			});
		});
	});

	describe('Find by email', () => {
		it('should return user', async () => {
			usersRepository.findOne = jest.fn().mockResolvedValue(mockUser);

			usersService.findByEmail(mockUser.email).then((result) => {
				expect(result).toEqual(mockUser);
			});
		});

		it('should return undefined', async () => {
			usersRepository.findOne = jest.fn().mockResolvedValue(undefined);

			usersService.findByEmail(mockUser.email).catch((err) => {
				expect(err.status).toEqual(HttpStatus.NOT_FOUND);
			});
		});
	});
});
