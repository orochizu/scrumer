import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { User } from '@scrumer/shared';
import { UserDto } from '@scrumer/shared';
import { isArray } from 'util';

@Injectable()
export class UsersService {
	constructor(
		@InjectRepository(User)
		private readonly users: Repository<User>
	) {}

	save(userDto: UserDto): Promise<User> {
		const user = this.users.create(userDto);
		return this.users.save(user);
	}

	saveAll(usersDto: UserDto[]): Promise<User[]> {
		const users = usersDto.map((userDto) => this.users.create(userDto));
		return this.users.save(users);
	}

	findByIds(ids: number[]): Promise<User[]> {
		return isArray(ids) && ids.length
			? this.users.findByIds(ids)
			: Promise.resolve([]);
	}

	findByEmail(email: string, isPasswordNeeded: boolean = false): Promise<User> {
		return this.users.findOne({
			...(isPasswordNeeded && { select: ['password'] }),
			where: { email },
		});
	}

	findAllByEmails(
		emails: string[],
		isPasswordNeeded: boolean = false
	): Promise<User[]> {
		return this.users.find({
			...(isPasswordNeeded && { select: ['password'] }),
			where: { email: In(emails) },
		});
	}
}
