import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { User } from '@scrumer/shared';
import { ConfigService } from '../../config/services/config.service';
import { UsersService } from '../../users/services/users.service';
import { JwtPayloadDto } from '@scrumer/shared';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	constructor(
		private readonly users: UsersService,
		private readonly configs: ConfigService
	) {
		super({
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
			secretOrKey: configs.getAuthSecretKey,
		});
	}

	async validate(jwtPayload: JwtPayloadDto): Promise<User> {
		const { email } = jwtPayload;
		return this.users.findByEmail(email).catch((err) => {
			Logger.error(err.message);
			throw new UnauthorizedException();
		});
	}
}
