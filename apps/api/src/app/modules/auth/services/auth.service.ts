import {
	Injectable,
	UnauthorizedException,
	UnprocessableEntityException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { comparePassword } from '../../../core/utils/compare-password';
import { ConfigService } from '../../config/services/config.service';
import { UsersService } from '../../users/services/users.service';
import { User } from '@scrumer/shared';
import { TokensService } from './tokens.service';
import {
	JwtTokenDto,
	EmailDto,
	TokenDto,
	UserDto,
	UserSigninDto,
} from '@scrumer/shared';

@Injectable()
export class AuthService {
	constructor(
		private readonly users: UsersService,
		private readonly tokens: TokensService,
		private readonly configs: ConfigService,
		private readonly jwt: JwtService
	) {}

	async signin(userSigninDto: UserSigninDto): Promise<JwtTokenDto> {
		const isValid = await this.validateCredentials(userSigninDto);
		if (!isValid) {
			throw new UnauthorizedException();
		}
		return this.generateJwtToken({ email: userSigninDto.email });
	}

	async signup(userDto: UserDto, tokenDto: TokenDto): Promise<User> {
		const token = await this.tokens.findValidByUuid(tokenDto.uuid);

		if (!token || token.email !== userDto.email) {
			throw new UnprocessableEntityException();
		}

		const isExist = !!(await this.users.findByEmail(userDto.email));

		if (isExist) {
			throw new UnauthorizedException();
		}

		await this.tokens.delete({ uuid: tokenDto.uuid });
		return this.users.save(userDto);
	}

	async validateCredentials(userSigninDto: UserSigninDto): Promise<boolean> {
		const user = await this.users.findByEmail(userSigninDto.email, true);
		return user && comparePassword(userSigninDto.password, user.password);
	}

	private generateJwtToken(recipient: EmailDto): JwtTokenDto {
		const expiresIn = this.configs.getAuthTokenExpiration;
		const token = this.jwt.sign(recipient, { expiresIn });
		return {
			expiresIn,
			timestamp: Date.now(),
			token,
		};
	}
}
