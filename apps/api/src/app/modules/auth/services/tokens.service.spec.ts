import { Test } from '@nestjs/testing';
import { TokensService } from './tokens.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Token } from '@scrumer/shared';
import { Repository } from 'typeorm';
import { EmailDto } from '@scrumer/shared';
import { MailerService } from '../../mailer/services/mailer.service';

const mockRecipient: EmailDto = {
	email: 'test@scrumer.com',
};

const mockToken = {
	id: 1,
	created: new Date(),
	expires: new Date(new Date().getDate() + 1),
	uuid: 'c816678b-6c8a-49df-9827-8fc9be94de0b',
};

const getMockTokensRepository = () => ({
	findOne: jest.fn(),
	delete: jest.fn(),
	create: jest.fn().mockReturnValue(mockToken),
	save: jest.fn().mockResolvedValue(mockToken),
});

const getMockMailerService = () => ({});

describe('TokensService', () => {
	let tokensService: TokensService;
	let mailerService: MailerService;
	let tokensRepository: Repository<Token>;

	beforeEach(async () => {
		const module = await Test.createTestingModule({
			providers: [
				TokensService,
				{
					provide: getRepositoryToken(Token),
					useFactory: getMockTokensRepository,
				},
				{
					provide: MailerService,
					useFactory: getMockMailerService,
				},
			],
		}).compile();

		tokensService = await module.get<TokensService>(TokensService);
		mailerService = await module.get<MailerService>(MailerService);
		tokensRepository = await module.get(getRepositoryToken(Token));
	});

	describe('Generate token', () => {
		it('should not call delete method', async () => {
			const mockDeleteListener = jest.spyOn(tokensRepository, 'delete');
			tokensRepository.findOne = jest.fn().mockResolvedValue(undefined);

			await tokensService.generateToken(mockRecipient);
			expect(mockDeleteListener.mock.calls.length).toBe(0);
		});

		it('should call delete method once', async () => {
			const mockDeleteListener = jest.spyOn(tokensRepository, 'delete');
			tokensRepository.findOne = jest.fn().mockResolvedValue(mockToken);

			await tokensService.generateToken(mockRecipient);
			expect(mockDeleteListener.mock.calls.length).toEqual(1);
		});

		it('should generate and return token', async () => {
			tokensRepository.findOne = jest.fn().mockResolvedValue(mockToken);

			const result = await tokensService.generateToken(mockRecipient);
			expect(result).toEqual(mockToken);
		});
	});
});
