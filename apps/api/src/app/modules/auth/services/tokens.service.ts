import { Injectable } from '@nestjs/common';
import { DeleteResult, FindConditions, MoreThan, Repository } from 'typeorm';
import { Token } from '@scrumer/shared';
import { getRegistrationTokenEmail } from '../../../core/utils/email-templates';
import { MailerService } from '../../mailer/services/mailer.service';
import { InjectRepository } from '@nestjs/typeorm';
import { EmailDto, TokenDto } from '@scrumer/shared';

@Injectable()
export class TokensService {
	constructor(
		@InjectRepository(Token)
		private readonly tokens: Repository<Token>,
		private readonly mailer: MailerService
	) {}

	async send(recipient: EmailDto): Promise<void> {
		const { uuid, email, expires } = await this.generateToken(recipient);
		this.mailer.sendMail({
			to: email,
			subject: 'Registration Token',
			html: getRegistrationTokenEmail(email, uuid, expires),
		});
	}

	async generateToken(recipient: EmailDto): Promise<Token> {
		const token = await this.tokens.findOne({
			email: recipient.email,
		});
		if (token) {
			const { uuid } = token;
			this.delete({ uuid });
		}
		return this.create(recipient);
	}

	async validateToken(tokenDto: TokenDto): Promise<boolean> {
		const { uuid } = tokenDto;
		const token = await this.tokens.findOne({
			where: { uuid, expires: MoreThan(new Date()) },
		});
		return !!token;
	}

	findValidByUuid(uuid: string): Promise<Token> {
		return this.tokens.findOne({
			where: { uuid, expires: MoreThan(new Date()) },
		});
	}

	create(token: EmailDto | Token): Promise<Token> {
		const t = this.tokens.create(token);
		return this.tokens.save(t);
	}

	delete(criteria: FindConditions<Token>): Promise<DeleteResult> {
		return this.tokens.delete(criteria);
	}
}
