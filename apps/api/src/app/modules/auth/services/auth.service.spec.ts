import { Test } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { UsersService } from '../../users/services/users.service';
import { ConfigService } from '../../config/services/config.service';
import { JwtService } from '@nestjs/jwt';
import { TokensService } from './tokens.service';
import { TokenDto, UserDto, UserRole, UserSigninDto } from '@scrumer/shared';
import { UnauthorizedException } from '@nestjs/common';
import { JwtTokenDto } from '@scrumer/shared';
import { genSaltSync, hashSync } from 'bcrypt';

const mockTokenDtoValid: TokenDto = {
	uuid: 'c816678b-6c8a-49df-9827-8fc9be94de0b',
};

const mockTokenValid = {
	created: new Date(),
	expires: new Date(new Date().getDate() + 1),
	email: 'test@scrumer.com',
	...mockTokenDtoValid,
};

const mockUserSigninDto: UserSigninDto = {
	email: 'test@scrumer.com',
	password: 'TestPassword',
};

const mockUserDto: UserDto = {
	nick: 'TestNick',
	name: 'TestName',
	surname: 'TestSurname',
	role: UserRole.REGULAR,
	email: 'test@scrumer.com',
	password: hashSync('TestPassword', genSaltSync()),
};

const mockUser = {
	id: 1,
	created: new Date(),
	updated: new Date(),
	...mockUserDto,
};

const mockJwtToken: JwtTokenDto = {
	token:
		'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.' +
		'eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.' +
		'SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
	timestamp: Date.now(),
	expiresIn: 86400,
};

const mockJwtService = () => ({
	sign: jest.fn().mockReturnValue(mockJwtToken.token),
});

const mockUsersService = () => ({});

const mockTokensService = () => ({
	findValidByUuid: jest.fn(),
	validateToken: jest.fn(),
	delete: jest.fn(),
});

const mockConfigService = () => ({
	getAuthTokenExpiration: mockJwtToken.expiresIn,
});

describe('AuthService', () => {
	let jwtService: JwtService;
	let authService: AuthService;
	let usersService: UsersService;
	let tokensService: TokensService;
	let configService: ConfigService;

	beforeEach(async () => {
		const module = await Test.createTestingModule({
			providers: [
				AuthService,
				{ provide: JwtService, useFactory: mockJwtService },
				{ provide: UsersService, useFactory: mockUsersService },
				{ provide: TokensService, useFactory: mockTokensService },
				{ provide: ConfigService, useFactory: mockConfigService },
			],
		}).compile();

		jwtService = await module.get<JwtService>(JwtService);
		authService = await module.get<AuthService>(AuthService);
		usersService = await module.get<UsersService>(UsersService);
		tokensService = await module.get<TokensService>(TokensService);
		configService = await module.get<ConfigService>(ConfigService);
	});

	describe('Creating new user - singup', () => {
		it('should return new user', async () => {
			tokensService.findValidByUuid = jest
				.fn()
				.mockResolvedValue(mockTokenValid);
			usersService.findByEmail = jest.fn().mockResolvedValue(undefined);

			usersService.save = jest.fn().mockResolvedValue(mockUser);

			const result = await authService.signup(mockUserDto, mockTokenDtoValid);
			expect(result).toEqual(mockUser);
		});

		it('should throw UnauthorizedException', async () => {
			tokensService.findValidByUuid = jest
				.fn()
				.mockResolvedValue(mockTokenValid);
			usersService.findByEmail = jest.fn().mockResolvedValue(mockUser);

			try {
				await authService.signup(mockUserDto, mockTokenDtoValid);
			} catch (e) {
				expect(e).toBeInstanceOf(UnauthorizedException);
			}
		});
	});

	describe('Login as user - singin', () => {
		it('should return JwtTokenDto object with defined token property', async () => {
			usersService.findByEmail = jest.fn().mockResolvedValue(mockUser);

			const result = await authService.signin(mockUserSigninDto);
			expect(result.token).toEqual(mockJwtToken.token);
		});

		it('should throw UnauthorizedException', async () => {
			usersService.findByEmail = jest.fn().mockResolvedValue(undefined);

			try {
				await authService.signin(mockUserSigninDto);
			} catch (e) {
				expect(e).toBeInstanceOf(UnauthorizedException);
			}
		});
	});
});
