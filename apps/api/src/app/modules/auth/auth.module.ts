import { Module } from '@nestjs/common';
import { UsersModule } from '../users/users.module';
import { ConfigModule } from '../config/config.module';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '../config/services/config.service';
import { PassportModule } from '@nestjs/passport';
import { AuthController } from './controllers/auth.controller';
import { JwtStrategy } from './providers/jwt.strategy';
import { AuthService } from './services/auth.service';
import { MailerModule } from '../mailer/mailer.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TokensService } from './services/tokens.service';
import { Token } from '@scrumer/shared';

@Module({
	imports: [
		UsersModule,
		ConfigModule,
		MailerModule,
		TypeOrmModule.forFeature([Token]),
		PassportModule.register({ defaultStrategy: 'jwt' }),
		JwtModule.registerAsync({
			imports: [ConfigModule],
			inject: [ConfigService],
			useFactory: (configService: ConfigService) => ({
				secret: configService.getAuthSecretKey,
			}),
		}),
	],
	exports: [AuthService, PassportModule],
	controllers: [AuthController],
	providers: [AuthService, TokensService, JwtStrategy],
})
export class AuthModule {}
