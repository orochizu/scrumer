import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from '../services/auth.service';
import { EmailDto, User } from '@scrumer/shared';
import { TokenDto, UserDto, UserSigninDto } from '@scrumer/shared';
import { JwtTokenDto } from '@scrumer/shared';
import { TokensService } from '../services/tokens.service';

@Controller('auth')
export class AuthController {
	constructor(
		private readonly auth: AuthService,
		private readonly tokens: TokensService
	) {}

	@Post('signin')
	async signin(@Body() userSigninDto: UserSigninDto): Promise<JwtTokenDto> {
		return this.auth.signin(userSigninDto);
	}

	@Post('signup')
	async signup(
		@Body('user') userDto: UserDto,
		@Body('token') tokenDto: TokenDto
	): Promise<User> {
		return this.auth.signup(userDto, tokenDto);
	}

	@Post('sendToken')
	async send(@Body() recipient: EmailDto): Promise<void> {
		return this.tokens.send(recipient);
	}

	@Post('validateToken')
	async validate(@Body() tokenDto: TokenDto): Promise<boolean> {
		return this.tokens.validateToken(tokenDto);
	}
}
