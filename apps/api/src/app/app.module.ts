import { Logger, Module, OnModuleInit } from '@nestjs/common';
import { AuthModule } from './modules/auth/auth.module';
import { ConfigModule } from './modules/config/config.module';
import { UsersModule } from './modules/users/users.module';
import { DatabaseModule } from './modules/database/database.module';
import { TasksModule } from './modules/tasks/tasks.module';
import { BoardsModule } from './modules/boards/boards.module';
import { UsersService } from './modules/users/services/users.service';
import { BoardsService } from './modules/boards/services/boards.service';
import { environment } from '../environments/environment';
import { usersMock } from '@scrumer/shared';
import { boardsMock } from '@scrumer/shared';

const MODULES = [
	ConfigModule,
	DatabaseModule,
	BoardsModule,
	UsersModule,
	TasksModule,
	AuthModule,
];

@Module({
	imports: [...MODULES],
})
export class AppModule implements OnModuleInit {
	constructor(
		private readonly users: UsersService,
		private readonly boards: BoardsService
	) {}

	onModuleInit(): void {
		if (!environment.production) {
			this.saveMockedDataToDatabase();
		}
	}

	private async saveMockedDataToDatabase(): Promise<void> {
		try {
			const mockedEmails = usersMock.map((userMock) => userMock.email);
			const existingUsers = await this.users.findAllByEmails(mockedEmails);
			if (existingUsers && existingUsers.length) {
				return;
			}

			const users = await this.users.saveAll(usersMock);
			const owners = boardsMock.flatMap((board) =>
				users.filter((user) => user.id === board.ownerId)
			);

			boardsMock.forEach((board, index) => {
				const { name, membersIds } = board;
				this.boards.save(owners[index], { name, membersIds });
			});
		} catch (e) {
			Logger.debug(
				`Issue occurred while loading mocked data to database: ${e.message}`
			);
		}
	}
}
