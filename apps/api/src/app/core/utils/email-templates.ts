export const getRegistrationTokenEmail = (
	email: string,
	tokenUuid: string,
	expires: Date
): string => `
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      href="https://fonts.googleapis.com/css?family=Roboto&display=swap"
      rel="stylesheet"
    />
    <title>Title</title>
    <style type="text/css">
      span {
        font-weight: bold;
      }
    </style>
  </head>
  <body
    style="
        height: 100vh;
        width: 100vw;
        margin: 0;
        font-family: 'Roboto', sans-serif;
        font-size: 1.20em;"
  >
    <table
      height="100%"
      width="100%"
      border="0"
      cellspacing="0"
      cellpadding="0"
    >
      <tr>
        <td align="center" valign="middle">
          <div
            style="
        max-width: 50rem;
        width: 50%;
        padding: 2rem;
        margin-left: auto;
        margin-right: auto;
        text-align: center;
        border-radius: 1rem;
        box-shadow: 0.1rem 0.1rem 1rem #232323, 0.1rem 0.1rem 1rem #232323;"
          >
            You have received a registration token.
            <br />
            <br />
            This token is assigned to
            <br />
            <span>${email}</span>
            <br />and can be used only once.
            <br />
            <br />
            <br />
            Your token: <span>${tokenUuid}</span>
            <br />
            Token is valid until: <span>${expires.toUTCString()}</span>
            <br />
          </div>
        </td>
      </tr>
    </table>
  </body>
</html>
`;
