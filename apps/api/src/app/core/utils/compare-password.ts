import * as bcrypt from 'bcrypt';

export const comparePassword = (password: string, hash: string): boolean => {
	return password ? bcrypt.compareSync(password, hash) : false;
};
