module.exports = {
  name: 'api',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/api',
  displayName: {
    name: 'API',
    color: 'cyan'
  }
};
